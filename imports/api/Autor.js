import {Mongo} from 'meteor/mongo';
import {SimpleSchema} from 'meteor/aldeed:simple-schema';
import {Sexo} from "./Sexo";
import './method/AutorMethods'


const Autor = new Mongo.Collection("autor");

const ObjAutor= new SimpleSchema({

   nome:{type:string,label:"Nome do Autor"},
   sexo:{type:Sexo,label:"Sexo do Autor"},
   codigo:{type:Number,label:"Codigo do Autor"}

});

Autor.schema=SimpleSchema({
    autor:{type:ObjAutor},
    userId:{type:Number,regEx:SimpleSchema.RegEx.Id}
});

export {Autor};