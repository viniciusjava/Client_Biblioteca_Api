import {Mongo} from 'meteor/mongo';
import {SimpleSchema} from 'meteor/aldeed:simple-schema';
import {Livro} from "./Livro";
import {Cliente} from "./Cliente";
import './method/EmprestimoMethods'


const Emprestimo = Mongo.Collection("emprestimo");

const ObjEmprestimo = new SimpleSchema({

    dataRegistro:{type:Date,label:"Data de criação da locação"},
    dataDevolucao:{type:Date,label:"Data para Devolver o Livro"},
    valorEmprestimo:{type:Number,label:"Valor de emprestimo do Livro por dia"}
    livro:{type:Livro,label:"Objeto Livro"},
    cliente:{type:Cliente,label:"Cliente"},
    codigo:{type:Number,label:"codigo do Emprestimo"}

});


Emprestimo.schema = new SimpleSchema({
   emprestimo:{type:Emprestimo},
   userId:{type:Number,regEx:SimpleSchema.RegEx.id}
});

export {Emprestimo};