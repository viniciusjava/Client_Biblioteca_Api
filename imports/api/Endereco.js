import {Mongo} from 'meteor/mongo';
import {SimpleSchema} from 'meteor/aldeed:simple-schema';
import './method/EnderecoMethods'


const Endereco = Mongo.Collection("endereco");

const ObjEndereco = new SimpleSchema ({

    rua:{type:String,label:"Rua "},
    bairro:{type:String,label:"Bairro"},
    quadra:{type:Number,label:"Quadra"},
    lote:{type:Number,label:"Lote"},
    numero:{type:Number,label:"Numero"},
    complemento:{type:String,label:"Complemento"},
    cidade:{type:String,label:"Cidade"},
    estado:{type:String,label:"Estado"},
    pais:{type:String,label:"Pais"},
    codigo:{type:Number,label:"ID do Endereco"}


});


Endereco.schema = SimpleSchema ({
   endereco:{type:Endereco},
   userId:{type:Number,regEx:SimpleSchema.RegEx.Id}
});

export {Endereco};