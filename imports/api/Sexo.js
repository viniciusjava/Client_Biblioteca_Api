import {Mongo} from 'meteor/mongo';
import {SimpleSchema} from 'meteor/aldeed:simple-schema';

const Sexo = new Mongo.Collection("sexo");

const ObjSexo = new SimpleSchema({
   masculino:{type:string,label:"MASCULINO"},
   feminino:{type:String,label:"FEMININO"},
   codigo:{type:Number,label:"Codido do Sexo"}
});

Sexo.schema = SimpleSchema ({
   sexo:{type:ObjSexo},
   userId:{Number,regEx:SimpleSchema.RegEx.Id}
});

export {Sexo};