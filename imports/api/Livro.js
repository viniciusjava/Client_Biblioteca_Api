import {Mongo} from 'meteor/mongo';
import {SimpleSchema} from 'meteor/aldeed:simple-schema';

import {Genero} from "./Genero";
import {Editora} from "./Editora";
import {Autor} from "./Autor";
import './method/LivroMethods'

const Livro = new Mongo.Collection("livro");

const ObjLivro = new SimpleSchema ({

   nome:{type:String,label:"Nome do livro"},
   volume:{type:Number,label:"Volume do livro"},
   dataDePublicacao:{type:Date,label:"Data em foi publicado"},
   valor:{type:Number,label:"Valor do livro para aluguel"},
   genero:{type:Genero,label:"Tipo de Genero"},
   editora:{type:Editora,label:"Nome da Editora"},
   autor:{type:Autor,label:"Nome do Autor"},
   codigo:{type:Number,label:"Codigo do Livro"}


});

Livro.schema = SimpleSchema({
   livro:{type:ObjLivro},
   userId:{type:Number, regEx:SimpleSchema.RegExp.Id}
});

export {Livro};