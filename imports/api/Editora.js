import {Mongo} from 'meteor/mongo';
import {SimpleSchema} from 'meteor/aldeed:simple-schema';
import './method/EditoraMethods'


const Editora = new Mongo.Collection("editora");

const ObjEditora = new SimpleSchema({
    nome:{type:String,label:"Nome da Editora"},
    codigo:{type:Number,label:"codigo da Editora"}
});

Editora.schema = SimpleSchema ({
    editora:{type:ObjEditora},
    userId:{type:Number,regEx:SimpleSchema.RegExp.Id}
});

export {Editora};