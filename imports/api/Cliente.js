import {Mongo} from 'meteor/mongo';
import {SimpleSchema} from 'meteor/aldeed:simple-schema';
import {Endereco} from "./Endereco";
import './method/ClienteMethods'


const Cliente = Mongo.Collection("cliente");

const ObjCliente = new SimpleSchema({

});


Cliente.schema = new SimpleSchema({

    nome:{type:String,label:"Nome do cliente "},
    cpf:{type:String,label:"Cpf do cliente "},
    email:{type:String,label:"Email do cliente"},
    telefone:{type:String,label:"Telefone do cliente"},
    endereco:{type:Endereco,label:"Endereco do cliente"},
    codigo:{type:Number,regEx:SimpleSchema.RegEx.Id}
});

Cliente.schema = new SimpleSchema({
    cliente:{type:ObjCliente},
    userId:{Number,regEx:SimpleSchema.RegEx.id}
});

export {Cliente};