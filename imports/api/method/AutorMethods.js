import {Meteor} from 'meteor/meteor';
import { HTTP } from 'meteor/http';


Meteor.methods({

    salvar(autor){insert(autor)},
    atualizar(autor){update(autor)},
    apagar(id){remove(id)},
    buscarRest() {if(Meteor.isServer) {this.unblock();
        const URL = "http://localhost:8080/autor/";return Meteor.wrapAsync(restCall)(URL);}}


});


const restCall = function (URL,callback) {
    try {
        const result = HTTP.get(URL);
        callback(null,result.data);
    }catch(e){
        console.log(e);
        callback(500,"Erro ao acessar API");
    }
};


function remove(id) {
    if(Meteor.isServer){
        try {
            const URL =`http://localhost:8080/autor/${id}`;
            HTTP.del(URL,function (error,response) {
                if (error){console.log(error);}else{console.log(response);}});
        }catch (e){console.log(e);}
    }

};



function insert(autor) {
    if(Meteor.isServer) {
        try {
            const URL = `http://localhost:8080/autor/`;
            console.log(autor);
            HTTP.post(URL, {data: autor}, function (error, response) {
                if (error) {console.log(error);} else {console.log(response);}});
        } catch (e) {console.log(e);}
    }
};

function update(autor){
    if(Meteor.isServer) {

        try {
            const URL = `http://localhost:8080/autor/${autor.id}`;
            HTTP.put(URL, {data: autor}, function (error, response) {
                if (error) {console.log(error);}else {console.log(response);}});
        } catch (e) {console.log(e);}
    }
}


