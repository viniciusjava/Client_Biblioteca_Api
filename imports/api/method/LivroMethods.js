import {Meteor} from 'meteor/meteor';
import { HTTP } from 'meteor/http';


Meteor.methods({

    salvar(livro){insert(livro)},
    atualizar(livro){update(livro)},
    apagar(id){remove(id)},
    buscarRest() {if(Meteor.isServer) {this.unblock();
        const URL = "http://localhost:8080/livro/";return Meteor.wrapAsync(restCall)(URL);}}


});


const restCall = function (URL,callback) {
    try {
        const result = HTTP.get(URL);
        callback(null,result.data);
    }catch(e){
        console.log(e);
        callback(500,"Erro ao acessar API");
    }
};


function remove(id) {
    if(Meteor.isServer){
        try {
            const URL =`http://localhost:8080/livro/${id}`;
            HTTP.del(URL,function (error,response) {
                if (error){console.log(error);}else{console.log(response);}});
        }catch (e){console.log(e);}
    }

};



function insert(livro) {
    if(Meteor.isServer) {
        try {
            const URL = `http://localhost:8080/livro/`;
            console.log(livro);
            HTTP.post(URL, {data: livro}, function (error, response) {
                if (error) {console.log(error);} else {console.log(response);}});
        } catch (e) {console.log(e);}
    }
};

function update(livro){
    if(Meteor.isServer) {

        try {
            const URL = `http://localhost:8080/livro/${autor.id}`;
            HTTP.put(URL, {data: livro}, function (error, response) {
                if (error) {console.log(error);}else {console.log(response);}});
        } catch (e) {console.log(e);}
    }
}


