import {Meteor} from 'meteor/meteor';
import { HTTP } from 'meteor/http';


Meteor.methods({

    salvar(editora){insert(editora)},
    atualizar(editora){update(editora)},
    apagar(id){remove(id)},
    buscarRest() {if(Meteor.isServer) {this.unblock();
        const URL = "http://localhost:8080/editora/";return Meteor.wrapAsync(restCall)(URL);}}


});


const restCall = function (URL,callback) {
    try {
        const result = HTTP.get(URL);
        callback(null,result.data);
    }catch(e){
        console.log(e);
        callback(500,"Erro ao acessar API");
    }
};


function remove(id) {
    if(Meteor.isServer){
        try {
            const URL =`http://localhost:8080/editora/${id}`;
            HTTP.del(URL,function (error,response) {
                if (error){console.log(error);}else{console.log(response);}});
        }catch (e){console.log(e);}
    }

};



function insert(editora) {
    if(Meteor.isServer) {
        try {
            const URL = `http://localhost:8080/editora/`;
            console.log(editora);
            HTTP.post(URL, {data: editora}, function (error, response) {
                if (error) {console.log(error);} else {console.log(response);}});
        } catch (e) {console.log(e);}
    }
};

function update(editora){
    if(Meteor.isServer) {

        try {
            const URL = `http://localhost:8080/editora/${autor.id}`;
            HTTP.put(URL, {data: editora}, function (error, response) {
                if (error) {console.log(error);}else {console.log(response);}});
        } catch (e) {console.log(e);}
    }
}


