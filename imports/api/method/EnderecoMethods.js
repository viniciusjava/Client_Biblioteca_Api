import {Meteor} from 'meteor/meteor';
import { HTTP } from 'meteor/http';


Meteor.methods({

    salvar(endereco){inserirEndereco(endereco)},
    atualizar(endereco){atualizarRest(endereco)},
    apagar(id){removerPorId(id)},
    buscarRest() {if(Meteor.isServer) {this.unblock();
    const URL = "http://localhost:8080/endereco/";return Meteor.wrapAsync(restCall)(URL);}}


});


const restCall = function (URL,callback) {
    try {
        const result = HTTP.get(URL);
        callback(null,result.data);
    }catch(e){
        console.log(e);
        callback(500,"Erro ao acessar API");
    }
};


function removerPorId(id) {
    if(Meteor.isServer){
        try {
            const URL =`http://localhost:8080/endereco/${id}`;
            HTTP.del(URL,function (error,response) {
               if (error){console.log(error);}else{console.log(response);}});
        }catch (e){console.log(e);}
    }

};



function inserirEndereco(endereco) {
    if(Meteor.isServer) {
        try {
            const URL = `http://localhost:8080/endereco/`;
            console.log(endereco);
            HTTP.post(URL, {data: endereco}, function (error, response) {
                if (error) {console.log(error);} else {console.log(response);}});
        } catch (e) {console.log(e);}
    }
};

function atualizarRest(endereco){
    if(Meteor.isServer) {

        try {
            const URL = `http://localhost:8080/endereco/${endereco.id}`;
            HTTP.put(URL, {data: endereco}, function (error, response) {
                if (error) {console.log(error);}else {console.log(response);}});
        } catch (e) {console.log(e);}
    }
}


