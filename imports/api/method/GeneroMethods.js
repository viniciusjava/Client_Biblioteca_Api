import {Meteor} from 'meteor/meteor';
import { HTTP } from 'meteor/http';

Meteor.methods({

    salvar(genero){insert(genero)},
    atualizar(genero){update(genero)},
    apagar(id){remove(id)},
    buscarRest() {if(Meteor.isServer) {this.unblock();
        const URL = "http://localhost:8080/genero/";return Meteor.wrapAsync(restCall)(URL);}}


});


const restCall = function (URL,callback) {
    try {
        const result = HTTP.get(URL);
        callback(null,result.data);
    }catch(e){
        console.log(e);
        callback(500,"Erro ao acessar API");
    }
};


function remove(id) {
    if(Meteor.isServer){
        try {
            const URL =`http://localhost:8080/genero/${id}`;
            HTTP.del(URL,function (error,response) {
                if (error){console.log(error);}else{console.log(response);}});
        }catch (e){console.log(e);}
    }

};



function insert(genero) {
    if(Meteor.isServer) {
        try {
            const URL = `http://localhost:8080/genero/`;
            console.log(genero);
            HTTP.post(URL, {data: genero}, function (error, response) {
                if (error) {console.log(error);} else {console.log(response);}});
        } catch (e) {console.log(e);}
    }
};

function update(genero){
    if(Meteor.isServer) {

        try {
            const URL = `http://localhost:8080/genero/${genero.id}`;
            HTTP.put(URL, {data: genero}, function (error, response) {
                if (error) {console.log(error);}else {console.log(response);}});
        } catch (e) {console.log(e);}
    }
}
