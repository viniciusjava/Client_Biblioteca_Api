import {Meteor} from 'meteor/meteor';
import { HTTP } from 'meteor/http';


Meteor.methods({

    salvar(cliente){insert(cliente)},
    atualizar(cliente){update(cliente)},
    apagar(id){remove(id)},
    buscarRest() {if(Meteor.isServer) {this.unblock();
        const URL = "http://localhost:8080/cliente/";return Meteor.wrapAsync(restCall)(URL);}}


});


const restCall = function (URL,callback) {
    try {
        const result = HTTP.get(URL);
        callback(null,result.data);
    }catch(e){
        console.log(e);
        callback(500,"Erro ao acessar API");
    }
};


function remove(id) {
    if(Meteor.isServer){
        try {
            const URL =`http://localhost:8080/cliente/${id}`;
            HTTP.del(URL,function (error,response) {
                if (error){console.log(error);}else{console.log(response);}});
        }catch (e){console.log(e);}
    }

};



function insert(cliente) {
    if(Meteor.isServer) {
        try {
            const URL = `http://localhost:8080/cliente/`;
            console.log(doador);
            HTTP.post(URL, {data: cliente}, function (error, response) {
                if (error) {console.log(error);} else {console.log(response);}});
        } catch (e) {console.log(e);}
    }
};

function update(cliente){
    if(Meteor.isServer) {

        try {
            const URL = `http://localhost:8080/cliente/${cliente.id}`;
            HTTP.put(URL, {data: cliente}, function (error, response) {
                if (error) {console.log(error);}else {console.log(response);}});
        } catch (e) {console.log(e);}
    }
}
