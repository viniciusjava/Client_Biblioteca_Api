import {Mongo} from 'meteor/mongo';
import {SimpleSchema} from 'meteor/aldeed:simple-schema';
import './method/GeneroMethods'


const Genero = new Mongo.Collection("genero");

const ObjGenero = new SimpleSchema ({

    nome:{type: String , label: "Nome do Genero"},
    codigo:{type:String , label:"Id do Genero"}
});


Genero.schema = SimpleSchema({
    genero:{type:ObjGenero},
    userId: {type: Number, regEx:SimpleSchema.Regex.id}
});

export {Genero};