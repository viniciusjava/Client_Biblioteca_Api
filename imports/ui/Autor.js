import {Template} from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';
import {Meteor} from 'meteor/meteor';
import {Tracker} from 'meteor/tracker';


Template.autor.onCreated(function () {

    this.estadoDaTela = new ReactiveDict();
    this.estadoDaTela.set('novo',false);
    this.estadoDaTela.set('ObjAutor',null);
    this.estadoDaTela.set('GetlistAutor',null);

    buscar(this);

    Tracker.autorun(() => {
        Meteor.subscribe('cadastroPorUsuario', Meteor.userId());
    });
});

Template.autor.helpers({
    listGenero(){return Template.instance().estadoDaTela.get('GetlistAutor');},
    mostrarForm(){return Template.instance().estadoDaTela.get('novo')},
    autor(){return Template.instance().estadoDaTela.get('ObjAutor')}
});


function buscar(instance) {
    Meteor.call('buscarRest',(error,response)=>{
        if (error){
            console.log(error);
        }else{
            console.log(response);
            instance.estadoDaTela.set('GetlistAutor',response)
        }
    });
}

function limparCampos() {$('.cadastro').trigger("reset");}
function instanciar(tipo,instance) {instance.estadoDaTela.set('novo',tipo);}
function  setRest(autor) {autor.nome = $('#').val();}
function createGenero() {const nome = $('#').val();const autor = {nome}
    return autor;}