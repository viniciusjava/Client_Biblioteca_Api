import {Template} from 'meteor/templating';
import { ReactiveDict } from 'meteor/reactive-dict';
import {Meteor} from 'meteor/meteor';
import {Tracker} from 'meteor/tracker';


Template.cliente.onCreated(function () {

    this.estadoDaTela = new ReactiveDict();
    this.estadoDaTela.set('novo',false);
    this.estadoDaTela.set('ObjGenero',null);
    this.estadoDaTela.set('GetlistGenero',null);

    buscar(this);

    Tracker.autorun(() => {
        Meteor.subscribe('cadastroPorUsuario', Meteor.userId());
    });
});

Template.cliente.helpers({
    listGenero(){return Template.instance().estadoDaTela.get('GetlistGenero');},
    mostrarForm(){return Template.instance().estadoDaTela.get('novo')},
    genero(){return Template.instance().estadoDaTela.get('ObjGenero')}
});


function buscar(instance) {
    Meteor.call('buscarRest',(error,response)=>{
        if (error){
            console.log(error);
        }else{
            console.log(response);
            instance.estadoDaTela.set('GetListGenero',response)
        }
    });
}

function limparCampos() {$('.cadastro').trigger("reset");}
function instanciar(tipo,instance) {instance.estadoDaTela.set('novo',tipo);}
function  setRest(genero) {genero.nome = $('#').val();}
function createGenero() {const nome = $('#').val();const genero = {nome}
return genero;}