import { Template } from 'meteor/templating';
import { ReactiveVar } from 'meteor/reactive-var';

import './main.html';



Template.registerHelper('isLogado', function() {
    return Meteor.userId();
});


Meteor.startup(() => {
    T9n.setLanguage('pt');
});

Template.body.events({
    'click .js-sair'(e) {
        e.preventDefault();
        AccountsTemplates.logout();
    }
});

